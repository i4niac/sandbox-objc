//
//  main.m
//  Sandbox-ObjC
//
//  Created by Maksym Grebenets on 27/10/2014.
//  Copyright (c) 2014 Maksym Grebenets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
