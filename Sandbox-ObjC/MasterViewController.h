//
//  MasterViewController.h
//  Sandbox-ObjC
//
//  Created by Maksym Grebenets on 27/10/2014.
//  Copyright (c) 2014 Maksym Grebenets. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

