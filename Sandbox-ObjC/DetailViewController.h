//
//  DetailViewController.h
//  Sandbox-ObjC
//
//  Created by Maksym Grebenets on 27/10/2014.
//  Copyright (c) 2014 Maksym Grebenets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

